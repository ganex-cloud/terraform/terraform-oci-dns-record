variable "compartment_id" {
  description = "(Optional) (Updatable) The OCID of the compartment the resource belongs to. If supplied, it must match the Zone's compartment ocid."
  type        = string
  default     = null
}

variable "domain" {
  description = "(Required) The fully qualified domain name where the record can be located. Domain value is case insensitive."
  type        = string
  default     = ""
}

variable "zone_name" {
  description = "(Required) The name of the target zone."
  type        = string
  default     = ""
}

variable "records" {
  description = "A List of simple records to add to the zone"
  type        = list(any)
  default     = []
}
