resource "oci_dns_record" "this" {
  count           = length(var.records)
  zone_name_or_id = var.zone_name
  domain          = lookup(var.records[count.index], "domain")
  compartment_id  = var.compartment_id
  rtype           = lookup(var.records[count.index], "rtype")
  rdata           = lookup(var.records[count.index], "rdata")
  ttl             = lookup(var.records[count.index], "ttl")
}
