module "oci_dns_record-example-com" {
  source    = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-dns-record.git?ref=master"
  zone_name = module.dns_zone_example-com.dns_zone_name
  records = [
    {
      domain = "example.com"
      rtype  = "TXT"
      rdata  = "v=spf1 include:_spf.google.com include:spf.oracleemaildelivery.com ~all"
      ttl    = 300
    },
    {
      domain = "example.com"
      rtype  = "MX"
      rdata  = "1 ASPMX.L.GOOGLE.COM"
      ttl    = 300
    },
    {
      domain = "example.com"
      rtype  = "MX"
      rdata  = "5 ALT1.ASPMX.L.GOOGLE.COM"
      ttl    = 300
    },
    {
      domain = "example.com"
      rtype  = "MX"
      rdata  = "5 ALT2.ASPMX.L.GOOGLE.COM"
      ttl    = 300
    },
    {
      domain = "example.com"
      rtype  = "MX"
      rdata  = "10 ALT3.ASPMX.L.GOOGLE.COM"
      ttl    = 300
    },
    {
      domain = "example.com"
      rtype  = "MX"
      rdata  = "10 ALT4.ASPMX.L.GOOGLE.COM"
      ttl    = 300
    },
    {
      domain = "teste1.example.com"
      rtype  = "A"
      rdata  = "8.8.8.8"
      ttl    = 3600
    },
    {
      domain = "teste2.example.com"
      rtype  = "CNAME"
      rdata  = "google.com"
      ttl    = 3600
    },
    {
      domain = "teste3.example.com"
      rtype  = "A"
      rdata  = "8.8.8.8"
      ttl    = 3600
    },
    {
      domain = "k8s.example.com"
      rtype  = "NS"
      rdata  = "ns-1463.awsdns-54.org"
      ttl    = 172800
    },
    {
      domain = "k8s.example.com"
      rtype  = "NS"
      rdata  = "ns-1627.awsdns-11.co.uk"
      ttl    = 172800
    },
    {
      domain = "k8s.example.com"
      rtype  = "NS"
      rdata  = "ns-78.awsdns-09.com"
      ttl    = 172800
    },
    {
      domain = "k8s.example.com"
      rtype  = "NS"
      rdata  = "ns-673.awsdns-20.net"
      ttl    = 172800
    }
  ]
}
